package cn.yywd_123.LightAPI;

/*
 *  Author: yywd_123
 *  Date: 2022/8/13
 */

import cn.yywd_123.LightAPI.command.Command;

import java.util.List;
import java.util.UUID;

public interface IModUtils {
	default String getLoaderVersion() {
		return null;
	}

	default void addMod(UUID uuid, IMod mod) {}

	default int getDisplayWidth(){return 0;}
	default int getDisplayHeight(){return 0;}

	default void drawString(String text, int x, int y, int color) {}
	default void drawString(String text, float x, float y, int color) {}

	default void printChatMessage(String message) {}
	default void sendChatMessage(String message) {}

	default boolean isKeyDown(int keyCode) {return false;}
	default boolean isAttackKeyDown() {return false;}
	default boolean isUseItemKeyDown() {return false;}
	default void clickMouseLeft() {}
	default void clickMouseRight() {}
	default void addCommand(Command command) {}

	default void setAlwaysRenderBlockList(List<Integer> alwaysRenderBlockList) {}
}
