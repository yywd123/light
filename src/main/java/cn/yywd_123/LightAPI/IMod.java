package cn.yywd_123.LightAPI;

/*
 *  Author: yywd_123
 *  Date: 2022/8/13
 */

import java.util.UUID;

public abstract class IMod {
	private static boolean isEnable = true;

	public abstract void ModInit(IModUtils modUtils);
	public void init(String uuid, IModUtils utils, IMod mod) {
		utils.addMod(UUID.fromString(uuid), mod);
	}
	public abstract void ModMain(IModUtils modUtils);

	public abstract String getModInformation();
	public abstract String[] getModName();
	public abstract String getModVersion();

	public boolean isEnable() {return isEnable;}
	public void setEnable(boolean status) {isEnable = status;}
	public void enable() {isEnable = true;}
	public void disable() {isEnable = false;}

	public abstract void update();
	public abstract void render();
	public abstract void stop();

	public abstract void setting(String[] args, int argStart);

	public abstract String getUsage();
}
