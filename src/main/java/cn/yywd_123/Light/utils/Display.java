package cn.yywd_123.Light.utils;

/*
 *  Author: yywd_123
 *  Date: 2022/8/11
 */

public class Display {
	//  下面的函数都是占位符 真正的代码在ClassTransformer.injectController里面热注入进去 不然无法编译
	public static int getDisplayWidth(){return 0;}
	public static int getDisplayHeight(){return 0;}

	public static void setWindowTitle(String title) {}

	public static void drawString(String text, int x, int y, int color) {}
	public static void drawString(String text, float x, float y, int color) {}
}
