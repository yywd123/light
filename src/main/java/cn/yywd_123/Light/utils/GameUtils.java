package cn.yywd_123.Light.utils;

/*
 *  Author: yywd_123
 *  Date: 2022/8/12
 */

public class GameUtils {
	//  下面的函数都是占位符 真正的代码在ClassTransformer.injectController里面热注入进去 不然无法编译

	public static void printChatMessage(String message) {}
	public static void sendChatMessage(String message) {}


	public static boolean isKeyDown(int keyCode) {return false;}
	public static boolean isAttackKeyDown() {return false;}
	public static boolean isUseItemKeyDown() {return false;}
	public static void clickMouseLeft() {}
	public static void clickMouseRight() {}
}
