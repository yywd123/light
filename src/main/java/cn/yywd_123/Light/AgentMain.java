package cn.yywd_123.Light;

/*
 *  Author: yywd_123
 *  Date: 2022/8/7
 */

import org.slf4j.LoggerFactory;

import java.lang.instrument.Instrumentation;

public class AgentMain {
	private static String agentArg;
	public static void premain(String arg, Instrumentation inst) {
		agentArg = arg;
		Thread.currentThread().setName("Light模组管理器");
		inst.addTransformer(new ClassTransformer(inst), inst.isRetransformClassesSupported());
	}

	public static String getArg() {return agentArg;}
}
