package cn.yywd_123.Light.main;

/*
 *  Author: yywd_123
 *  Date: 2022/8/8
 */


import cn.yywd_123.Light.AgentMain;
import cn.yywd_123.Light.command.CommandManager;
import cn.yywd_123.LightAPI.IMod;
import cn.yywd_123.Light.mod.ModManager;
import cn.yywd_123.Light.utils.Client;
import cn.yywd_123.Light.utils.Display;
//import cn.yywd_123.Light.utils.item.ItemUtils;
import cn.yywd_123.Light.utils.GameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LightMain {
	public static final Logger logger = LoggerFactory.getLogger(LightMain.class);

	public static String UserName;

	public static final CommandManager commandManager = new CommandManager();
	public static final ModManager modManager = new ModManager();

	public static final Renderer renderer = new Renderer();

	public static boolean isWaterMarkEnable = false;
	
	public static void init() {
		logger.info("[Light] 模组管理器已被成功加载 作者:yywd_123");
		logger.info("[Light] 正在初始化中...");

		modManager.loadMods();
		commandManager.Init();

		logger.info("[Light] 初始化完成!");
	}

	public static void start(String[] args) {
		if (!AgentMain.getArg().equalsIgnoreCase(Client.getLaunchedVersion())) throw new IllegalArgumentException("[Light] 请传入正确的版本号!");
		UserName = args[0];

		logger.debug("玩家名称: " + args[0]);
		Display.setWindowTitle("Minecraft " + Client.getLaunchedVersion() + " | Light模组加载器 作者:yywd_123");
	}

	public static void onUpdate() {
		modManager.getEnabledMods().forEach(IMod::update);
	}

	public static void reload() {
		modManager.reload();
		commandManager.close();

		modManager.loadMods();
		commandManager.Init();

		GameUtils.printChatMessage("§a[Light] §6模组加载器热重载成功");
	}

	public static void stop() {
		commandManager.close();
		modManager.close();

		logger.info("[Light] 模组加载器已成功关闭 期待您的下次使用!");
	}


	public static String getUserName() {return UserName;}

	public static String getLoaderVersion() {
		return LightMain.class.getPackage().getImplementationVersion();
	}
}
