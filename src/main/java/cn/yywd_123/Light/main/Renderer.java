package cn.yywd_123.Light.main;

/*
 *  Author: yywd_123
 *  Date: 2022/9/3
 */

import cn.yywd_123.Light.utils.Block;
import cn.yywd_123.Light.utils.Display;
import cn.yywd_123.LightAPI.IMod;

import java.util.ArrayList;
import java.util.List;

public class Renderer {

	private List<Integer> alwaysRenderBlockList = new ArrayList<>();
	private boolean isRenderFilterEnabled = false;

	public void onRender() {
		LightMain.modManager.getEnabledMods().forEach(IMod::render);

		if (LightMain.isWaterMarkEnable) Display.drawString("作者:yywd_123 本视频仅在B站发布", 0, 0, 0xffffffff);
	}

	public List<Integer> getAlwaysRenderBlockList() {return alwaysRenderBlockList;}

	public void setAlwaysRenderBlockList(List<Integer> alwaysRenderBlockList) {
		this.alwaysRenderBlockList = alwaysRenderBlockList;
		setRenderFilterEnable(true);
	}

	public boolean isRenderFilterEnabled() {return this.isRenderFilterEnabled;}

	public void setRenderFilterEnable(boolean isEnable) {
		isRenderFilterEnabled = isEnable;
		loadRenderers();
	}

	public boolean shouldBlockSideBeRendered(Object block) {
		if (!isRenderFilterEnabled) return true;

		for (int i : alwaysRenderBlockList.toArray(new Integer[]{})) {
			if (block == Block.getBlockById(i)) return true;
		}

		return false;
	}

	public void loadRenderers() {}
}
