package cn.yywd_123.Light.main.utils.codeMap;

/*
 *  Author: yywd_123
 *  Date: 2022/8/19
 */

public class TransformMethod {
	private String action;
	private String transformTarget;
	private String transformSource;

	public String getAction() {
		return action;
	}

	public String getTransformTarget() {
		return transformTarget;
	}

	public String getTransformSource() {
		return transformSource;
	}
}
