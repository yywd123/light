package cn.yywd_123.Light.main.utils;

/*
 *  Author: yywd_123
 *  Date: 2022/8/13
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class JarLoader extends URLClassLoader {
	public JarLoader(URL[] urls, ClassLoader parent) {
		super(urls, parent);
	}

	public void addJar(File jarFile) throws MalformedURLException {
		this.addURL(jarFile.toURI().toURL());
	}
}
