package cn.yywd_123.Light.main.utils.codeMap;

/*
 *  Author: yywd_123
 *  Date: 2022/8/18
 */

public class CodeMap {
	private String checksum;
	private TransformMethod[] transformMethods;
	private TransformField[] transformFields;

	public String getChecksum() {
		return checksum;
	}

	public TransformMethod[] getTransformMethods() {
		return transformMethods;
	}
	public TransformField[] getTransformFields() {return transformFields;}
}
