package cn.yywd_123.Light.main.utils.codeMap;

/*
 *  Author: yywd_123
 *  Date: 2022/8/18
 */

public class GameVersionInfo {
	private String gameVersion;
	private String mapPath;

	public String getGameVersion() {
		return gameVersion;
	}

	public String getMapPath() {
		return mapPath;
	}
}
