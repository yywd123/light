package cn.yywd_123.Light.main.utils.codeMap;

/*
 *  Author: yywd_123
 *  Date: 2022/8/18
 */

public class MapIndex {
	private String indexVersion;
	private GameVersionInfo[] indexes;

	public GameVersionInfo getVersionInfo(String gameVersion) {
		for (GameVersionInfo info : indexes) if (info.getGameVersion().equals(gameVersion)) return info;
		return null;
	}

	public String getIndexVersion() {return this.indexVersion;}
}
