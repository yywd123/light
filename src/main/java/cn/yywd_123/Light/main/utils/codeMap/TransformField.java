package cn.yywd_123.Light.main.utils.codeMap;

/*
 *  Author: yywd_123
 *  Date: 2022/8/30
 */

public class TransformField {
	private String transformTarget;
	private String permission;

	public String getTransformTarget() {
		return transformTarget;
	}

	public String getPermission() {
		return permission;
	}
}
