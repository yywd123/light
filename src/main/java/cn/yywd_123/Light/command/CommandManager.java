package cn.yywd_123.Light.command;

/*
 *  Author: yywd_123
 *  Date: 2022/8/12
 */

import cn.yywd_123.Light.command.commands.*;
import cn.yywd_123.LightAPI.command.Command;

import java.util.HashMap;
import java.util.Map;

public class CommandManager {
	private final Map<String[], Command> commandMap = new HashMap<>();

	public boolean msgCheckCommand(String message){
		if(message.charAt(0) == '~'){
			String[] args = message.substring(1).split(" ");
			if(args[0].isEmpty()) return false;
			Command command = getCommand(args[0]);
			if (command == null) {
				return false;
			}else command.exec(args);

			return true;
		}//写啥？看看mcp有什么，oko'k 我以前写过一段时间客户端 我抄一点过来66666 呗好像我。。forge看不懂 找乐子罢（（（

		return false;
	}

	public Command getCommand(String key){
		for (Map.Entry<String[], Command> Entry : commandMap.entrySet()) {
			for (String commandName : Entry.getKey()) {
				if(commandName.equals(key)) return Entry.getValue();
			}
		}
		return null;
	}

	public void Init(){
		loadAllCommands();
	}

	public void loadAllCommands(){
		loadCommand(new HelpCommand());
		loadCommand(new ModManagerCommand());
		loadCommand(new ReloadCommand());
		loadCommand(new ProjectInfoCommand());
	}

	public void loadCommand(Command command) {
		commandMap.put(command.getKey(), command);
	}

	public String[] getAllCommands(){
		String[] commands = new String[commandMap.size()];
		int i = 0;
		for (Map.Entry<String[], Command> Entry : commandMap.entrySet()) {
			commands[i++] = "~" + Entry.getKey()[0];
		}
		return commands;
	}

	public void close() {
		commandMap.clear();
	}
}
