package cn.yywd_123.Light.command.commands;

/*
 *  Author: yywd_123
 *  Date: 2022/8/13
 */

import cn.yywd_123.LightAPI.command.Command;
import cn.yywd_123.Light.main.LightMain;
import cn.yywd_123.LightAPI.IMod;
import cn.yywd_123.Light.utils.GameUtils;

public class ModManagerCommand extends Command {
	public ModManagerCommand() {
		super(new String[]{"mod", "m"});
	}

	@Override
	public void exec(String[] args) {
		if(args.length < 3) {
			showUsage();
			return;
		}
		if(args[1].equalsIgnoreCase("all")){
			if (args[2].equalsIgnoreCase("enable") || args[2].equalsIgnoreCase("e")) {
				LightMain.modManager.getMods().forEach(IMod::enable);
				GameUtils.printChatMessage("§a[Light] §6所有模组的状态被成功设定为启用");
			}
			else if (args[2].equalsIgnoreCase("disable") || args[2].equalsIgnoreCase("d")) {
				LightMain.modManager.getMods().forEach(IMod::disable);
				GameUtils.printChatMessage("§a[Light] §6所有模组的状态被成功设定为禁用");
			}
			else {
				GameUtils.printChatMessage("§a[Light] §c无效的模组状态");
			}
		}else {
			IMod mod = LightMain.modManager.getMod(args[1]);
			if (mod == null) {
				GameUtils.printChatMessage("§a[Light] §c无效的模组");
			} else {
				if (args[2].equalsIgnoreCase("help") || args[2].equalsIgnoreCase("h")) {
					GameUtils.printChatMessage("§a[Light] §6以下是" + mod.getModName()[0] + "的用法:\n" + mod.getUsage());
					return;
				}
				boolean isEnable;
				if (args[2].equalsIgnoreCase("enable") || args[2].equalsIgnoreCase("e")) isEnable = true;
				else if (args[2].equalsIgnoreCase("disable") || args[2].equalsIgnoreCase("d")) isEnable = false;
				else {
					if (args[2].equalsIgnoreCase("set") || args[2].equalsIgnoreCase("s")) {
						mod.setting(args, 3);
						return;
					}
					GameUtils.printChatMessage("§a[Light] §c无效的操作");
					return;
				}
				mod.setEnable(isEnable);
				GameUtils.printChatMessage("§a[Light] §6模组" + mod.getModName()[0] + "的状态被成功设定为" + (isEnable ? "启用" : "禁用"));
			}
		}
	}

	public void showUsage(){
		GameUtils.printChatMessage(
				"§a[Light] §6mod:模组管理\n" +
						"§6用法: ~mod(或~m) 模组名称 [enable | disable]\n" +
						"      §6~mod(或~m) 模组名称 set 变量名 值\n");
		if (LightMain.modManager.getAvailableMods().length != 0) {
			GameUtils.printChatMessage("§6可用的模组有:");
			for (IMod mod : LightMain.modManager.getEnabledMods()) {
				GameUtils.printChatMessage("  §6" + mod.getModName()[0] + "    " + mod.getModInformation());
			}
		}
		else GameUtils.printChatMessage("§6当前无可用模组");
	}
}
