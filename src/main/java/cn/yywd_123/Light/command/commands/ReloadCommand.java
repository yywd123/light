package cn.yywd_123.Light.command.commands;

/*
 *  Author: yywd_123
 *  Date: 2022/8/15
 */

import cn.yywd_123.LightAPI.command.Command;
import cn.yywd_123.Light.main.LightMain;
import cn.yywd_123.Light.utils.GameUtils;

public class ReloadCommand extends Command {
	public ReloadCommand() {
		super(new String[]{"reload"});
	}

	boolean warned = false;
	@Override
	public void exec(String[] args) {
		if (!warned) {
			GameUtils.printChatMessage("§a[Light] §c您确定要让模组加载器热重载吗 这可能会使游戏崩溃!\n      §c如果要继续 请再次执行~reload命令");
			warned = true;
			return;
		}
		LightMain.reload();
	}
}
