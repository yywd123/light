package cn.yywd_123.Light.command.commands;

/*
 *  Author: yywd_123
 *  Date: 2022/8/16
 */

import cn.yywd_123.LightAPI.command.Command;
import cn.yywd_123.Light.main.LightMain;
import cn.yywd_123.Light.utils.Client;
import cn.yywd_123.Light.utils.GameUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProjectInfoCommand extends Command {
	public ProjectInfoCommand() {
		super(new String[]{"project-info", "info"});
	}

	List<Integer> list = Arrays.asList(7, 14, 15, 16, 21, 56, 73, 74, 153);

	@Override
	public void exec(String[] args) {
		if (args.length > 1) {
			if (args[1].equalsIgnoreCase("enable") || args[1].equalsIgnoreCase("e")) {
				LightMain.isWaterMarkEnable = true;
				LightMain.renderer.setAlwaysRenderBlockList(list);
				return;
			}
			else if (args[1].equalsIgnoreCase("disable") || args[1].equalsIgnoreCase("d")) {
				LightMain.isWaterMarkEnable = false;
				LightMain.renderer.setRenderFilterEnable(false);
				return;
			}
		}

		GameUtils.printChatMessage("§a[Light] §6当前版本: " + LightMain.getLoaderVersion() + " | 游戏版本" + Client.getLaunchedVersion());
		GameUtils.printChatMessage("     §6Copyright (C) 2020-2022 yywd_123");

		GameUtils.printChatMessage("     §6这是一个初中生写的模组加载器awa 求支持");
		GameUtils.printChatMessage("     §6仓库地址: https://gitee.com/yywd123/light");
	}
}
