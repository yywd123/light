package cn.yywd_123.Light.command.commands;

/*
 *  Author: yywd_123
 *  Date: 2022/8/12
 */

import cn.yywd_123.LightAPI.command.Command;
import cn.yywd_123.Light.main.LightMain;
import cn.yywd_123.Light.utils.GameUtils;

import java.util.Arrays;

public class HelpCommand extends Command {
	public HelpCommand() {
		super(new String[]{"help", "h"});
	}

	@Override
	public void exec(String[] args) {
		GameUtils.printChatMessage("§a[Light] §6可用的命令有:");

		GameUtils.printChatMessage("§6" + Arrays.toString(LightMain.commandManager.getAllCommands()).substring(1).replace(']', ' '));
	}
}