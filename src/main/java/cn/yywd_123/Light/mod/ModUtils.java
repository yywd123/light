package cn.yywd_123.Light.mod;

/*
 *  Author: yywd_123
 *  Date: 2022/8/13
 */

import cn.yywd_123.LightAPI.command.Command;
import cn.yywd_123.Light.main.LightMain;
import cn.yywd_123.Light.utils.Display;
import cn.yywd_123.Light.utils.GameUtils;
import cn.yywd_123.LightAPI.IMod;
import cn.yywd_123.LightAPI.IModUtils;

import java.util.List;
import java.util.UUID;

public class ModUtils implements IModUtils {
	//  给mod使用的API

	public String getLoaderVersion() {
		return LightMain.getLoaderVersion();
	}

	@Override
	public void addMod(UUID modUUID, IMod mod) {
		LightMain.modManager.addMod(modUUID, mod);
	}

	@Override
	public int getDisplayWidth() {
		return Display.getDisplayWidth();
	}

	@Override
	public int getDisplayHeight() {
		return Display.getDisplayHeight();
	}

	@Override
	public void drawString(String text, int x, int y, int color) {
		Display.drawString(text, x, y, color);
	}

	@Override
	public void drawString(String text, float x, float y, int color) {
		Display.drawString(text, x, y, color);
	}

	@Override
	public void printChatMessage(String message) {
		GameUtils.printChatMessage(message);
	}

	@Override
	public void sendChatMessage(String message) {
		GameUtils.sendChatMessage(message);
	}

	@Override
	public boolean isKeyDown(int keyCode) {
		return GameUtils.isKeyDown(keyCode);
	}

	@Override
	public boolean isAttackKeyDown() {
		return GameUtils.isAttackKeyDown();
	}

	@Override
	public boolean isUseItemKeyDown() {
		return GameUtils.isUseItemKeyDown();
	}

	@Override
	public void clickMouseLeft() {
		GameUtils.clickMouseLeft();
	}

	@Override
	public void clickMouseRight() {
		GameUtils.clickMouseRight();
	}

	@Override
	public void addCommand(Command command) {LightMain.commandManager.loadCommand(command);}

	@Override
	public void setAlwaysRenderBlockList(List<Integer> alwaysRenderBlockList) {LightMain.renderer.setAlwaysRenderBlockList(alwaysRenderBlockList);}
}
